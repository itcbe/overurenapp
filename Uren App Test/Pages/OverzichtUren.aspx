﻿<%@ Page language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderId="PlaceHolderAdditionalPageHead" runat="server">
        <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />
    <link rel="Stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css"/>
    <!-- Add JS library's here -->
    <script type="text/javascript" src="../Scripts/jquery-1.8.2.min.js"></script>

    <script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="../Scripts/Models/Registratie.js"></script>
    <script type="text/javascript" src="../Scripts/DateConversion.js"></script>
    <script type="text/javascript" src="../Scripts/Models/Overuren.js"></script>
    <script type="text/javascript" src="../Scripts/Views/CalendarViewModel.js"></script>

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript">
        
    </script>

</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Overzicht uren
</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
    <div class="Calender-Div">
        <select id="werknemerSelect">
            <option value="Carlo Daniels">Carlo Daniels</option>
            <option value="Davy Craenen">Davy Craenen</option>
            <option value="Davy Janssen">Davy Janssen</option>
            <option value="Dietmar Braem">Dietmar Braem</option>
            <option value="Frank Vanrusselt">Frank Vanrusselt</option>
            <option value="Glenn Vanderborght">Glenn Vanderborght</option>
            <option value="Koen Henderickx">Koen Henderickx</option>
            <option value="Kristof Dirkx">Kristof Dirkx</option>
            <option value="Kristof Herten">Kristof Herten</option>
            <option value="Kristof Peters">Kristof Peters</option>
            <option value="Mario Derese">Mario Derese</option>
            <option value="Martijn  Valkenborgh">Martijn Valkenborgh</option>
            <option value="Ron Stevens">Ron Stevens</option>
            <option value="Stefan Cardinaels">Stefan Cardinaels</option>
            <option value="Stefan Kelchtermans">Stefan Kelchtermans</option>        
        </select>
        <select id="maandSelect">
            <option value="01">Januari</option>
            <option value="02">Februari</option>
            <option value="03">Maart</option>
            <option value="04">April</option>
            <option value="05">Mei</option>
            <option value="06">Juni</option>
            <option value="07">Juli</option>
            <option value="08">Augustus</option>
            <option value="09">September</option>
            <option value="10">Oktober</option>
            <option value="11">November</option>
            <option value="12">December</option>
        </select>
        <select id="jaarSelect">
            <option value="2020">2020</option>
            <option value="2019">2019</option>
            <option value="2018">2018</option>
            <option value="2017">2017</option>
            <option value="2016">2016</option>
            <option value="2015">2015</option>
            <option value="2014">2014</option>
            <option value="2013">2013</option>
            <option value="2012">2012</option>
            <option value="2011">2011</option>
            <option value="2010">2010</option>
            <option value="2009">2009</option>
            <option value="2008">2008</option>
            <option value="2007">2007</option>
            <option value="2006">2006</option>
            <option value="2005">2005</option>
            <option value="2004">2004</option>
        </select>
        <a href="#" onclick="return CreateCalendar();" class="Buttons">Toon</a>
        <table id="calendarTable">
            <thead>
                <tr>
                    <th>Ma</th>
                    <th>Di</th>
                    <th>Wo</th>
                    <th>Do</th>
                    <th>Vr</th>
                    <th>Za</th>
                    <th>Zo</th>
                </tr>
            </thead>
            <tbody id="calendar">
                <tr>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                    <td>
                        <div class="dag">
                            <h4></h4>
                            <h5></h5>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="totalenDiv">
        <span class="totalen" id="totaalMaandLabel"></span>
        <span class="totalen" id="totaalLabel"></span>
    </div>
    <div class="overlay"></div>
    <div class="popup">
        <div id="closediv"><img src="../Images/closebox.png" onclick="HidePopup();" width="20" alt="close" /></div>
        <div class="popupContent">
            <table id="detailTable">
                <tbody id="detailTBody">
                               
                </tbody>
            </table>
        </div>
        <div class="footerCloseDiv">
            <a href="#" class="Buttons" onclick="HidePopup();">Sluiten</a>
        </div>
    </div>
</asp:Content>

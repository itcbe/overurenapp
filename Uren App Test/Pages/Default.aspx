﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/datepicker-nl.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="../Scripts/DateConversion.js"></script>
    <script type="text/javascript" src="../Scripts/Views/DrempelwaardeViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Views/UsersViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Views/GetTijdsregistratiesViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Models/Overuren.js"></script>
    <script type="text/javascript" src="../Scripts/Models/VerlofLijstItem.js"></script>
    <script type="text/javascript" src="../Scripts/Models/Rendabel.js"></script>
    <script type="text/javascript" src="../Scripts/Views/OverurenViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Models/UpdateVerlofLijst.js"></script>
    <script type="text/javascript" src="../Scripts/Models/Registratie.js"></script>


    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />
    <link rel="Stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/App.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Uren
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
    </div>
<%--    <div style="margin-bottom: 2em;">
        <a href="OverzichtUren.aspx" class="Buttons">Overzicht uren</a>
    </div>--%>
    <div class="wrapper">
        <h3>Rendabel</h3>
        <div>
            <fieldset>
                <label for="medewerkerSelect" class="rendabelFieldsetLabel">Werknemer:</label>
                <select id="medewerkerSelect" onchange="medewerkerSelect_OnChange();"></select>
                <label for="startdatumTextBox" class="rendabelFieldsetLabel">Startdatum</label>
                <input type="text" class="datepicker" id="startdatumTextBox" />
                <label for="einddatumTextBox" class="rendabelFieldsetLabel">Einddatum</label>
                <input type="text" class="datepicker" id="einddatumTextBox" />
                <a onclick="BerekenButton_Click();" class="Buttons">Bereken</a>
            </fieldset>
            <div id="spinner">
                <img src="../Images/374.GIF" alt="Berekenen van de gegevens..." />
            </div>
            <div class="clear"></div>
        </div>
        <h4>Tijdsegistraties volledig ingevuld</h4>
        <div id="dagDiv">

        </div>
        <div class="clear"></div>
        <h4>Rendabel per dag</h4>
        <div id="dagRendabel">

        </div>
        <div class="clear"></div>
        <h4>Sales</h4>
        <div id="salesDiv">

        </div>
        <div class="clear"></div>
        <h4>Rendabel in de geselecteerde periode</h4>
        <div id="rendabelDiv">
            <div id="rendabelBlok" onclick="showInfoDialog();">
                <p><span id="percentageP">0%</span></p>
            </div>
        </div>
        <div class="blok">
            <h3>Tijdsregistraties</h3>
            <table>
                <tbody>
                    <tr>
                        <td>Werknemer:</td>
                        <td>
                            <select id="werknemerSelect"></select>
                        </td>
                        <td>Datum:</td>
                        <td>
                            <input id="datumTextBox" type="text" class="datepicker"/>
                        </td>
                        <td><a class="Buttons" onclick="return GetTijdsregistraties();">Update</a></td>
                        <td><a id="SaveButton" class="Buttons" onclick="return GetRegistratiesFromList();">Opslaan</a></td>
                    </tr>
                </tbody>
            </table>
            <table id="tijdsregistratieTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ticket</th>
                        <th></th>
                        <th>Startuur</th>
                        <th>Einduur</th>
                        <th>Duur</th>
                        <th>Omschrijving</th>
                        <th>Status</th>
                        <th>Onsite</th>
                        <th>Rendabel</th>
                        <th>Permanentie</th>
                        <th>Te factureren uren</th>
                        <th>Eenheidsprijs</th>
                        <th>Klant</th>
                        <th>Gemaakt door</th>
                    </tr>
                </thead>
                <tbody id="registratieTBody">
                </tbody>

            </table>
            <%--            Totaal uren: <span id="totaalLabel"></span>--%>
        </div>
        <div id="dialog" title="Berekening informatie.">
            <p id="infoTextP"></p>
        </div>
<%--        <div class="blok2">
            <h3>Overuren</h3>
            <table id="OverurenTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tijdsregistratie ID</th>
                        <th>Aantal</th>
                        <th>Datum</th>
                        <th>Nagekeken</th>
                        <th>Werknemer</th>
                    </tr>
                </thead>
                <tbody id="OverurenTbody">
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3"><span style="color: #0094ff; font-size: 1.2em;" id="totaalLabel"></span></td>
                        <td colspan="2"><span style="color: #0094ff; font-size: 1.2em;">Overuren: </span>
                            <input id="overurenTXT" type="text" style="width: 50px" /></td>
                    </tr>
                </tfoot>
            </table>
            <table>
                <tbody>
                    <tr>
                        <td><a id="saveButton" class="Buttons" onclick="return SaveOveruren();">Opslaan</a></td>
                        <td><a id="checkAndSaveButton" class="Buttons" onclick="return CheckAllAndSave();">Dag nagekeken en opslaan</a></td>
                        <td><a id="showAllButton" class="Buttons" onclick="return ShowAllOveruren();">Alle overuren</a></td>
                        <td><a id="HerberekenButton" class="Buttons" onclick="return HerberekenUren();">Herbereken</a></td>
                    </tr>
                </tbody>
            </table>
            <p class="ErrorLabels" id="ErrorLabel"></p>
        </div>--%>
    </div>
    <div id="Loader">
        <div id="overlay"></div>
        <div id="SaveSpinner">
            <img src="../Images/374.GIF" />
        </div>
    </div>

</asp:Content>

﻿function ToShortDateString(datum) {
    var datumArray = datum.split(' ');
    var dag = datumArray[1];
    var jaar = datumArray[3];
    var maand = "";
    var uur = "";
    if (datumArray.length > 4)
        uur = datumArray[4];
    switch (datumArray[2]) {
        case "januari":
            maand = "01";
            break;
        case "februari":
            maand = "02";
            break;
        case "maart":
            maand = "03";
            break;
        case "april":
            maand = "04";
            break;
        case "mei":
            maand = "05";
            break;
        case "juni":
            maand = "06";
            break;
        case "juli":
            maand = "07";
            break;
        case "augustus":
            maand = "08";
            break;
        case "september":
            maand = "09";
            break;
        case "oktober":
            maand = "10";
            break;
        case "november":
            maand = "11";
            break;
        case "december":
            maand = "12";
            break;
    }

    var result = dag + "/" + maand + "/" + jaar;
    return result;
}

function DateToString(date) {
    var dag = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    return dag + "/" + (month < 10 ? "0" + month : month) + "/" + year;
}

function DateToReverseString(date) {
    var dag = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    return year + "/" + (month < 10 ? "0" + month : month) + "/" + dag;
}

//function GetDateString(datum) {
//    var datumArray = datum.split(' ');
//    var dag = datumArray[1];
//    var jaar = datumArray[3];
//    var maand = "";
//    var uur = "";
//    if (datumArray.length > 4)
//        uur = datumArray[4];
//    switch (datumArray[2]) {
//        case "januari":
//            maand = "01";
//            break;
//        case "februari":
//            maand = "02";
//            break;
//        case "maart":
//            maand = "03";
//            break;
//        case "april":
//            maand = "04";
//            break;
//        case "mei":
//            maand = "05";
//            break;
//        case "juni":
//            maand = "06";
//            break;
//        case "juli":
//            maand = "07";
//            break;
//        case "augustus":
//            maand = "08";
//            break;
//        case "september":
//            maand = "09";
//            break;
//        case "oktober":
//            maand = "10";
//            break;
//        case "november":
//            maand = "11";
//            break;
//        case "december":
//            maand = "12";
//            break;
//    }

//    var result = dag + "/" + maand + "/" + jaar;
//    if (uur !== "")
//        result += " " + uur;
//    return result;
//}

function DateTimeToString(date) {
    return date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear() + " " + date.getHours() + ":" + (date.getMinutes() < 10 ? ("0" + date.getMinutes()) : date.getMinutes()) + ":00";
}

function stringToDate(date, time) {
    var dateArray = date.split('/');
    var result = "";
    var day = dateArray[0];
    var tempmonth = parseInt(dateArray[1], 10) - 1;
    var month = tempmonth < 10 ? "0" + tempmonth : tempmonth;
    var yearTime = dateArray[2].split(' ');
    var year = yearTime[0];
    if (time) {
        var myTime = yearTime[1].split(":");
        var hour = myTime[0];
        var min = myTime[1];
        return new Date(year, month, day, hour, min)
    }
    else
        return new Date(year, month , day);
}

function ToSharePointDate(date) {
    var dateArray = date.split('/');
    var result = "";
    var day = dateArray[0];
    var month = dateArray[1];
    var yearTime = dateArray[2].split(' ');
    var year = yearTime[0];
    var time = yearTime[1];
    if (time) {
        var timeArray = time.split(":");
        var hour = timeArray[0];
        var min = timeArray[1];
        hour = hour;// - 2;
        if (hour < 10) {
            hour = "0" + hour;

        }
        result = year + "-" + month + "-" + day + "T" + hour + ":" + min + ":00";
    }
    else
        result = year + "-" + month + "-" + day + "T02:00:00";

    return result;
}

function GetTime(date) {

}

function DateToSharePointDate(date) {
    var year = date.getFullYear();
    var month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    var minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();

    return year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":00";
}

function IsSafari() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf('safari') > -1 && ua.indexOf('edge') == -1) {
        return true;
    }
    else
        return false;
}

function IsEdge() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf('edge') > -1)
        return true;
    else
        return false;
}

function IsChrome() {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf('chrome') > -1) {
        return true;
    }
    else
        return false;
}




function GetDateString(datum, datepicker) {
    var dag = "";
    var maand = "";
    var jaar = "";
    var uur = "";
    if ($.browser.mozilla) {
        var dagMaandArray = datum.split('-');
        var jaarUurArray = dagMaandArray[2].split(' ');
        dag = dagMaandArray[0];
        maand = dagMaandArray[1] < 10 ? "0" + dagMaandArray[1] : dagMaandArray[1];
        jaar = jaarUurArray[0];
        if (jaarUurArray.length > 1)
            uur = jaarUurArray[1];
    }
    else if (IsSafari()) {
        var dagMaandArray = datum.split(' ');
        var temp = dagMaandArray[2];
        dag = dagMaandArray[0];
        maand = "";
        uur = "";
        switch (dagMaandArray[1]) {
            case "januari":
                maand = "01";
                break;
            case "februari":
                maand = "02";
                break;
            case "maart":
                maand = "03";
                break;
            case "april":
                maand = "04";
                break;
            case "mei":
                maand = "05";
                break;
            case "juni":
                maand = "06";
                break;
            case "july":
                maand = "07";
                break;
            case "augustus":
                maand = "08";
                break;
            case "september":
                maand = "09";
                break;
            case "oktober":
                maand = "10";
                break;
            case "november":
                maand = "11";
                break;
            case "december":
                maand = "12";
                break;
        }
        jaar = dagMaandArray[2];
        if (dagMaandArray.length > 3)
            uur = dagMaandArray[3];
    }
    else if (IsEdge()) {
        var datumArray = datum.split(' ')[0].split('/');
        var daggetal = datumArray[0];
        var dag = "";

        if (daggetal < 10)
            dag = "0" + datumArray[0];
        else
            dag = datumArray[0];

        var maand = datumArray[1]
        var jaar = datumArray[2];
        var uur = datum.split(' ')[1];
    }
    else if (IsChrome()) {
        var datumArray = datum.split('-');
        dag = datumArray[0];
        maand = datumArray[1] < 10 ? "0" + datumArray[1] : datumArray[1];
        var jaarTijd = datumArray[2].split(' ');
        jaar = jaarTijd[0];
        uur = "";
        if (jaarTijd.length > 1)
            uur = jaarTijd[1];


    }
    else {
        var datumArray = datum.split(' ');
        dag = datumArray[1];
        jaar = datumArray[3];
        maand = "";
        uur = "";
        if (datumArray.length > 4)
            uur = datumArray[4];
        switch (datumArray[2]) {
            case "januari":
                maand = "01";
                break;
            case "februari":
                maand = "02";
                break;
            case "maart":
                maand = "03";
                break;
            case "april":
                maand = "04";
                break;
            case "mei":
                maand = "05";
                break;
            case "juni":
                maand = "06";
                break;
            case "juli":
                maand = "07";
                break;
            case "augustus":
                maand = "08";
                break;
            case "september":
                maand = "09";
                break;
            case "oktober":
                maand = "10";
                break;
            case "november":
                maand = "11";
                break;
            case "december":
                maand = "12";
                break;
        }
    }

    var result = "";
    if (datepicker)
        result += dag + "/" + maand + "/" + jaar;
    else
        result += jaar + "/" + maand + "/" + dag;
    if (uur !== "")
        result += " " + uur;
    return result;
}

function getQueryStringParameter(paramToRetrieve) {
    var params =
    document.URL.split("?")[1].split("&");
    var strParams = "";
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve) {
            var url = singleParam[1];
            url = url.replace(/%3A/g, ':');
            url = url.replace(/%2F/g, '/');
            url = url.replace(/%2E/g, '.');
            url = url.replace(/%5F/g, '_');
            return url;
        }
    }
}

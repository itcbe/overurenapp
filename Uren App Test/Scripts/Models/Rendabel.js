﻿var registraties = [];
var tarieven = [];
var specialePrijzen = [];
var dagopbrengsten = [];
var verplaatsingsKosten = [];


function GetRegistraties(startdatum, einddatum, medewerker, minwaarde, maxwaarde) {
    self = this;
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

        var caml = "<View><Query><Where><And>";
        caml += "<And>";
        caml += "<And>";
        caml += "<And>";
        caml += "<Geq><FieldRef Name='Startuur' /><Value Type='DateTime'>" + startdatum + "</Value></Geq>";
        caml += "<Leq><FieldRef Name='Einduur' /><Value Type='DateTime'>" + einddatum + "</Value></Leq>";
        caml += "</And>";
        caml += "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + minwaarde + "</Value></Gt>";
        caml += "</And>";
        caml += "<Leq><FieldRef Name='ID' /><Value Type='Counter'>" + maxwaarde + "</Value></Leq>";
        caml += "</And>";
        caml += "<Eq><FieldRef Name='Author' /><Value Type='User'>" + medewerker + "</Value></Eq>";
        caml += "</And>";
        caml += "</Where>";
        caml += "<OrderBy><FieldRef Name='Startuur' /></OrderBy>";
        caml += "</Query>";
        caml += "<ViewFields>";
        caml += "<FieldRef Name='ID' />";
        caml += "<FieldRef Name='Ticket_x0020_getal' />";
        caml += "<FieldRef Name='Title' />";
        caml += "<FieldRef Name='Startuur' />";
        caml += "<FieldRef Name='Einduur' />";
        caml += "<FieldRef Name='Eenheidsprijs' />";
        caml += "<FieldRef Name='Duur' />";
        caml += "<FieldRef Name='Uren' />";
        caml += "<FieldRef Name='Te_x0020_factureren_x0020_uren' />";
        caml += "<FieldRef Name='Type_x0020_werk' />";
        caml += "<FieldRef Name='Onsite' />";
        caml += "<FieldRef Name='Rendabel' />";
        caml += "<FieldRef Name='Status' />";
        caml += "<FieldRef Name='Opm_x0020_facturatie' />";
        caml += "<FieldRef Name='Korte_x0020_omschrijving' />";
        caml += "<FieldRef Name='Klant' />";
        caml += "<FieldRef Name='Klant_x003a_Postcode' />";
        caml += "<FieldRef Name='Author' />";
        caml += "</ViewFields>";
        caml += "<RowLimit>500</Rowlimit>";
        caml += "</View>";


        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        function () {
            var listEnumerator = self._pendingItems.getEnumerator();
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();
                var startuur = GetDateString(item.get_item('Startuur').toLocaleString());
                var einduur = GetDateString(item.get_item('Einduur').toLocaleString());
                var klantitem = item.get_item('Klant');
                var klant = "";
                if (klantitem !== undefined && klantitem !== null)
                    klant = klantitem.get_lookupValue();
                registraties.push(
                {
                    ID: item.get_item('ID'),
                    Ticket: item.get_item('Ticket_x0020_getal'),
                    Startuur: startuur,
                    Einduur: einduur,
                    Duur: item.get_item('Duur'),
                    Uren: item.get_item('Te_x0020_factureren_x0020_uren'),
                    Status: item.get_item('Status'),
                    Onsite: item.get_item('Onsite'),
                    Rendabel: item.get_item('Rendabel'),
                    Medewerker: item.get_item('Author').get_lookupValue(),
                    Werktype: item.get_item('Type_x0020_werk'),
                    Prijs: item.get_item('Eenheidsprijs') == null ? "" : item.get_item('Eenheidsprijs'),
                    Klant: klant,
                    KlantPostcode: item.get_item('Klant_x003a_Postcode')
                });
            }

            if (maxwaarde < maxDrempelWaarde) {
                minwaarde = maxwaarde;
                maxwaarde += 4999;
                GetRegistraties(startdatum, einddatum, medewerker, minwaarde, maxwaarde);
            }
            else {
                GetTarieven();
                dfd.resolve();
            }

        },
        function (sender, args) {
            alert('Kan de registraties niet laden!!: ' + args.get_message() + '\n' + args.get_stackTrace());
            dfd.reject();
        });
    });
    return dfd.promise();
}

function GetTarieven() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Werknemers');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><Query>"
            + "<Where><Eq><FieldRef Name='Actief' /><Value Type='Boolean'>1</Value></Eq></Where>"
            + "</Query>"
            + "<ViewFields>"
              + "<FieldRef Name='ID' />"
              + "<FieldRef Name='Title' />"
              + "<FieldRef Name='Prijs_x0020_hosting' />"
              + "<FieldRef Name='Prijs_x0020_werk' />"
              + "<FieldRef Name='Prijs_x0020_garantie' />"
              + "<FieldRef Name='Prijs_x0020_helpdesk' />"
              + "<FieldRef Name='Categorie' />"
              + "<FieldRef Name='Dagopbrengst' />"
           + "</ViewFields>"
           + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        tarieven = [];
        var listEnumerator = self._pendingItems.getEnumerator();
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            tarieven.push(
            {
                ID: item.get_item('ID'),
                Naam: item.get_item('Title'),
                PrijsWerk: item.get_item('Prijs_x0020_werk'),
                PrijsHelpDesk: item.get_item('Prijs_x0020_helpdesk'),
                Categorie: item.get_item('Categorie').get_lookupValue(),
                Dagopbrengst: item.get_item('Dagopbrengst')
            });
        }
        GetSpecialePrijzen();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de tarieven niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function GetSpecialePrijzen() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Speciale prijzen klanten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><ViewFields>"
                              + "<FieldRef Name='ID' />"
                              + "<FieldRef Name='Klant' />"
                              + "<FieldRef Name='Geldig_x0020_vanaf' />"
                              + "<FieldRef Name='Nieuwe_x0020_eenheidsprijs' />"
                              + "<FieldRef Name='Specifiek_x0020_voor_x0020_ticke' />"
                             + " <FieldRef Name='Prijs_x0020_voor' />"
                           + "</ViewFields></View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        specialePrijzen = [];
        var listEnumerator = self._pendingItems.getEnumerator();
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            specialePrijzen.push(
            {
                ID: item.get_item('ID'),
                Klant: item.get_item('Klant').get_lookupValue(),
                PrijsVoor: item.get_item('Prijs_x0020_voor'),
                Prijs: item.get_item('Nieuwe_x0020_eenheidsprijs'),
                SpecifiekVoorTicket: item.get_item('Specifiek_x0020_voor_x0020_ticke'),
                GeldigVanaf: item.get_item('Geldig_x0020_vanaf')
            });
        }
        GetTravelinPrices();

    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de speciale tarieven niet laden!!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function GetTravelinPrices() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Verplaatsingkosten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
            + "<Query>"
             + "<OrderBy><FieldRef Name='Postcode' /></OrderBy>"
            + "</Query>"
             + "<ViewFields><FieldRef Name='ID' /><FieldRef Name='Title' /><FieldRef Name='Postcode' /><FieldRef Name='vkp' /></ViewFields>"
            + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            verplaatsingsKosten.push(
            {
                ID: item.get_item('ID'),
                Code: item.get_item('Title'),
                Postcode: item.get_item('Postcode'),
                Prijs: item.get_item('vkp')
            });
        }
        SetRendabel();
        SetDagBlokjes();
        GetTijdsregistraties();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function SetRendabel() {
    var waarde = GetValueA();
    var kleur = "transparent";
    if ((waarde * 100) >= 100)
        kleur = "green";
    else if ((waarde * 100) >= 85)
        kleur = "orange";
    else if ((waarde * 100) < 85 && (waarde * 100) > 0)
        kleur = "red";

    $("#rendabelBlok").css("background-color", kleur);
    $("#percentageP").text(Math.round((waarde * 10000) / 100) + "%");
}

function GetValueA() {
    if (registraties.length > 0) {
        var totaalprijs = 0;
        var subtotaal = 0;
        var dagen = 1;
        var dagopbrengst = GetDagopbrengst(registraties[0].Medewerker);
        var dagtotaal = 0;
        var dag = registraties[0].Startuur.split("/")[2].split(' ')[0];
        var speciaal = false;
        var verlaatsing = false;
        var ziek = false;
        var factureren = false;
        var dagom = false;
        var tellen = true;
        var verlof = 0;
        var verloftotaal = 0;
        infoText += "<h4>" + registraties[0].Startuur.split(' ')[0] + ".</h4>";
        for (var i = 0; i < registraties.length; i++) {
            var huidigeDag = registraties[i].Startuur.split("/")[2].split(' ')[0];
            if (huidigeDag != dag) {
                dag = registraties[i].Startuur.split("/")[2].split(' ')[0];
                totaalprijs += subtotaal;

                dagen++;
                if (subtotaal !== 0) {
                    infoText += "<p style='color: #0171C5;'>Dagtotaal: " + Math.round(subtotaal * 100) / 100 + " / " + Math.round(dagopbrengst * 100) / 100 + ".</p>";
                }
                infoText += "<h4>" + registraties[i].Startuur.split(' ')[0] + ".</h4>";
                subtotaal = 0;

                verloftotaal += verlof > 8 ? 8 : verlof;
                verlof = 0;
            }

            if (registraties[i].Ticket == "2") {
                verlof += parseFloat(registraties[i].Duur);
            }

            var waarde = GetRendabel(registraties[i], true);
            subtotaal += waarde;

            if (i == registraties.length - 1)
                totaalprijs += subtotaal;
        }

        if (subtotaal !== 0) {
            infoText += "<p style='color: #0171C5;'>Dagtotaal: " + Math.round(subtotaal * 100) / 100 + " / " + Math.round(dagopbrengst * 100) / 100 + ".</p><hr>";
        }
        verloftotaal += verlof > 8 ? 8 : verlof;
        dagen = dagen - (verloftotaal * 0.125);

        infoText += "Totaal periode: " + Math.round(totaalprijs * 100) / 100 + ".<br/>";
        infoText += "Verwacht: " + (Math.round(dagopbrengst * 100) / 100) * dagen + ".<br/>";
        infoText += "<br />Verlofuren: " + Math.round(verloftotaal * 100) / 100 + " uren.<br />";


        return totaalprijs > 0 ? (totaalprijs / dagen) / dagopbrengst : 0;
    }
    else
        return 0;
}

function GetRendabel(regis, totaal) {
    var prijs = 0;
    var verplaatsingsKost = 0;
    var functie = "";
    var rendabel = false;
    speciaal = false;
    verplaatsing = false;
    ziek = false;
    factureren = false;
    var dag = regis.Startuur.split("/")[2].split(' ')[0];
    var duur = 0;
    if (regis.Uren != null && regis.Uren != "")
        duur = parseFloat(regis.Uren);
    else
        duur = parseFloat(regis.Duur);
    var tellen = true;

    if (regis.Ticket === "1")
        verplaatsing = true;
    else if (regis.Ticket === "3" || regis.Ticket === "4")
        ziek = true;
    else if (parseInt(regis.Ticket) <= 20) {
        speciaal = true;
    }

    if (ziek) {
        prijs = 0;
    }
    else if (verplaatsing) {
        prijs = 0;
    }
    else if (speciaal && regis.Rendabel != "Ja") {
        prijs = 0;
    }
    else {
        if (regis.Rendabel === "Ja" || (regis.Status == "Afgewerkt" && regis.Rendabel != "Nee")) {
            //if ((regis.Status == "Factureren"
            //    || regis.Status == "Wacht - Factureerbaar"
            //    || regis.Status == "Volgende facturatiemaand factureren"
            //    || (regis.Status == "Afgewerkt" && regis.Rendabel != "Nee"))
            //    && (!verplaatsing && !ziek)
            //    || (speciaal && regis.Rendabel != "Nee" && (regis.Status == "Factureren"
            //        || regis.Status == "Wacht - Factureerbaar"
            //        || regis.Status == "Volgende facturatiemaand factureren"
            //        || regis.Status == "Afgewerkt"))) {
            factureren = true;
        }
        else
            if (!speciaal)
                tellen = false;

        // 1 ophalen van het tarief van de medewerker
        for (var j = 0; j < tarieven.length; j++) {
            if (tarieven[j].Naam === regis.Medewerker) {
                prijs = parseFloat(tarieven[j].PrijsWerk);
                dagopbrengst = parseFloat(tarieven[j].Dagopbrengst)
                functie = tarieven[j].Categorie;
                break;
            }
        }
        // 2 kijken of er speciale prijzen zijn voor de klant
        //if (speciaal == false) {
        if (factureren && tellen) {
            if (regis.Onsite === "Ja") {
                for (var m = 0; m < verplaatsingsKosten.length; m++) {
                    if (regis.KlantPostcode.get_lookupValue() === verplaatsingsKosten[m].Postcode) {
                        verplaatsingsKost = verplaatsingsKosten[m].Prijs;
                        break;
                    }
                }
                //infoText += "Verplaatsing kost: " + verplaatsingsKost + ".<br/>"
            }
            if (regis.Werktype !== "Hosting" || regis.Werktype !== "Garantie") {
                if (regis.Prijs == "") {
                    for (var k = 0; k < specialePrijzen.length; k++) {
                        if (specialePrijzen[k].Klant === regis.Klant) {
                            for (var l = 0; l < specialePrijzen[k].PrijsVoor.length; l++) {
                                if (specialePrijzen[k].PrijsVoor[l].get_lookupValue() === functie) {
                                    prijs = parseFloat(specialePrijzen[k].Prijs);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                    prijs = parseFloat(regis.Prijs);
            }
            // 3 en 4 voeg de prijs * duur toe aan de totaalprijs

        }
        if (totaal) {
            infoText += "Ticket: " + regis.Ticket + ".<br/>";
            infoText += "Rendabel: " + regis.Rendabel + ".<br/>";
            infoText += "Status: " + regis.Status + ".<br/>";
            infoText += "Prijs: " + prijs + ".<br/>";
            infoText += "Duur: " + Math.round(duur * 100) / 100 + ".<br />";
            if (verplaatsingsKost > 0)
                infoText += "Verplaatsing: " + verplaatsingsKost + ".<br />";
            infoText += "Totaal: " + Math.round((factureren ? prijs * duur + verplaatsingsKost : 0) * 100) / 100 + ".<br/><br/>";
        }

    }
    return factureren ? prijs * duur + verplaatsingsKost : 0;
}

function GetDagopbrengst(Medewerker) {
    var dagopbrengst = 0;
    for (var j = 0; j < tarieven.length; j++) {
        if (tarieven[j].Naam === Medewerker) {
            //prijs = parseFloat(tarieven[j].PrijsWerk);
            dagopbrengst = parseFloat(tarieven[j].Dagopbrengst)
            //functie = tarieven[j].Categorie;
            break;
        }
    }
    return dagopbrengst;
}

function GetStandaardPrijs(medewerker) {
    var prijs = 0;
    for (var j = 0; j < tarieven.length; j++) {
        if (tarieven[j].Naam === medewerker) {
            prijs = parseFloat(tarieven[j].PrijsWerk);
            break;
        }
    }
    return prijs;
}

function SetDagBlokjes() {
    $('#salesDiv').empty();
    $("#dagDiv").empty();
    $('#dagRendabel').empty();
    var startdatum = $('#startdatumTextBox').datepicker('getDate');
    var einddatum = $('#einddatumTextBox').datepicker('getDate');//, false);
    //var prijs = 0;
    //if (registraties.length > 0)
    //    prijs = GetStandaardPrijs(registraties[0].Medewerker)
    var dagen = (((((einddatum - startdatum) / 1000) / 60) / 60) / 24);
    var zomerdatum = new Date(2016, 2, 27, 0, 0, 0, 0);
    if (startdatum < zomerdatum && einddatum > zomerdatum)
        dagen += 1;
    var currentDate = startdatum;
    var first = true;
    for (var d = 0; d <= dagen; d++) {
        var currentdag = currentDate.getDate();
        var currentMonth = currentDate.getMonth();
        var currentYear = currentDate.getFullYear();
        var dagduur = 0;
        var rendabelwaarde = 0;
        var dagopbrengst = 0;
        var salesTijd = 0;
        var salesBedrag = 0;
        var verloftijd = 0;
        if (registraties.length > 0)
            dagopbrengst = GetDagopbrengst(registraties[0].Medewerker);
        for (var i = 0; i < registraties.length; i++) {

            var jaar = registraties[i].Startuur.split('/')[0];
            var maand = registraties[i].Startuur.split('/')[1];
            var thismonth = maand - 1;
            var dag = registraties[i].Startuur.split('/')[2].split(' ')[0];
            var today = new Date();
            today.setYear(jaar);
            today.setMonth(thismonth);
            today.setDate(dag);
            if (currentdag == dag && currentMonth == thismonth && currentYear == jaar) {
                dagduur += parseFloat(GetDuur(registraties[i]));
                rendabelwaarde += GetRendabel(registraties[i], false);
                if (registraties[i].Ticket === "2") {
                    verloftijd += parseFloat(registraties[i].Duur);
                }
                if (registraties[i].Status === "Sales") {
                    var tijd = 0;
                    if (registraties[i].Uren !== "") {
                        tijd = parseFloat(registraties[i].Uren);
                    }
                    else
                        tijd = parseFloat(registraties[i].Duur);

                    salesTijd += tijd;
                    salesBedrag += parseFloat(registraties[i].Prijs) * tijd;
                }
            }
        }

        var blokje = "";
        var vandaag = DateToString(currentDate);
        var daguren = DayIsComplete(currentDate, registraties);
        var compleet = NoHoles(currentDate, registraties);
        if (currentDate.getDay() > 0 && currentDate.getDay() < 6) {
            if (Math.round(/*dagduur*/ daguren * 100) / 100 >= 8 && compleet) {
                if (currentdag === 1 || first) {
                    blokje = GetFirstDayBlock("green", currentdag, currentDate.getMonth(), vandaag);

                }
                else
                    blokje = GetBlock("green", currentdag, vandaag);
            }
            else if (daguren === 0) {
                if (currentdag === 1 || first) {
                    blokje = GetFirstDayBlock("transparent", currentdag, currentDate.getMonth(), vandaag);
                    if (first) first = false;
                }
                else
                    blokje = GetBlock("transparent", currentdag, vandaag);
            }
            else {
                if (currentdag === 1 || first) {
                    blokje = GetFirstDayBlock("red", currentdag, currentDate.getMonth(), vandaag);
                    if (first) first = false;
                }
                else
                    blokje = GetBlock("red", currentdag, vandaag);
            }
        }
        else
            if (currentdag === 1 || first) {
                blokje = GetFirstDayBlock("gray", currentdag, currentDate.getMonth(), vandaag);
                if (first) first = false;
            }
            else
                blokje = GetBlock("gray", currentdag, vandaag);

        // Toon rendabel blokje.

        var rendabelBlokje = "";
        verloftijd = verloftijd > 8 ? 8 : verloftijd;
        var waarde = (rendabelwaarde / (dagopbrengst - (dagopbrengst * (verloftijd * 0.125)))) * 100;
        if (currentDate.getDay() > 0 && currentDate.getDay() < 6) {
            if (waarde >= 100) {
                if (currentdag === 1 || first) {
                    rendabelBlokje = GetFirstDayBlock("green", currentdag, currentDate.getMonth(), vandaag);
                }
                else
                    rendabelBlokje = GetBlock("green", currentdag, vandaag);
            } else if (waarde >= 85) {
                if (currentdag === 1 || first) {
                    rendabelBlokje = GetFirstDayBlock("orange", currentdag, currentDate.getMonth(), vandaag);
                }
                else
                    rendabelBlokje = GetBlock("orange", currentdag, vandaag);
            }
            else if (waarde > 0)
                if (currentdag === 1 || first) {
                    rendabelBlokje = GetFirstDayBlock("red", currentdag, currentDate.getMonth(), vandaag);
                }
                else
                    rendabelBlokje = GetBlock("red", currentdag, vandaag);
            else {
                if (currentdag === 1 || first) {
                    rendabelBlokje = GetFirstDayBlock("transparent", currentdag, currentDate.getMonth(), vandaag);
                }
                else
                    rendabelBlokje = GetBlock("transparent", currentdag, vandaag);
            }
        }
        else
            if (currentdag === 1 || first) {
                rendabelBlokje = GetFirstDayBlock("gray", currentdag, currentDate.getMonth(), vandaag);
                if (first) first = false;
            }
            else
                rendabelBlokje = GetBlock("gray", currentdag, vandaag);

        var salesblokje = "";
        if (currentdag === 1 || first) {
            salesblokje = GetFirstSalesBlok(currentdag, salesTijd, salesBedrag, currentMonth)
        }
        else
            salesblokje = GetSalesBlock(currentdag, salesTijd, salesBedrag);
        $('#salesDiv').append(salesblokje);
        $('#dagRendabel').append(rendabelBlokje);
        $("#dagDiv").append(blokje);
        currentDate.setDate(currentDate.getDate() + 1);
        if (first) first = false;
    }
    $("#Loader").hide();
    //$('#spinner').hide();
}

function Blok_Click(datum) {
    $('#datumTextBox').val(datum);
    var naam = $('#werknemerSelect').val();
    var vandaag = ToSharePointDate(datum);
    GetAllTijdsregistraties(vandaag, naam)
    //getTijdsregistraties(vandaag, naam);
}

function SetRendabelBlokjes() {
    //$('#')

}

function GetSalesBlock(dag, tijd, bedrag) {
    if (bedrag == 0) {
        return "<div class='salesBlok'></div>";
    }
    else {
        return "<div class='salesBlok' title='Duur: " + Math.round(tijd * 100) / 100 + "\nBedrag: " + Math.round(bedrag * 100) / 100 + " €'><span>" + Math.round(tijd * 100) / 100 + "</span><br /><span>" + Math.round(bedrag * 100) / 100 + "</span></div>";
    }
}

function GetFirstSalesBlok(dag, tijd, bedrag, maand) {
    var maandnaam = GetStringMaand(maand);

    if (bedrag == 0) {
        return "<div class='maandDiv'>" + maandnaam + "</div><div class='salesBlok'></div>";
    }
    else {
        return "<div class='maandDiv'>" + maandnaam + "</div><div class='salesBlok' title='Duur: " + Math.round(tijd * 100) / 100 + "\nBedrag: " + Math.round(bedrag * 100) / 100 + " €'><span>" + Math.round(tijd * 100) / 100 + "</span><br /><span>" + Math.round(bedrag * 100) / 100 + "</span></div>";
    }

}

function GetBlock(kleur, dag, datum) {
    return "<div style='width:30px; height:20px; cursor:grab; float: left; display: inline-block; margin:5px 3px; border: 1px solid black; background-color: " + kleur + "; padding:3px; ' onclick='Blok_Click(\"" + datum + "\");'>" + dag + "</div>";
}

function GetFirstDayBlock(kleur, dag, maand, datum) {
    var maandnaam = GetStringMaand(maand);
    return "<div class='maandDiv'>" + maandnaam + "</div><div style='width:30px; cursor:grab; height:20px; float: left; display: inline-block; margin:5px 3px; border: 1px solid black; background-color: " + kleur + "; padding:3px; ' onclick='Blok_Click(\"" + datum + "\");'>" + dag + "</div>";
}

function GetStringMaand(maand) {
    var maandnaam = "";
    switch (maand) {
        case 0:
            maandnaam = "Januari";
            break;
        case 1:
            maandnaam = "Februari";
            break;
        case 2:
            maandnaam = "Maart";
            break;
        case 3:
            maandnaam = "April";
            break;
        case 4:
            maandnaam = "Mei";
            break;
        case 5:
            maandnaam = "Juni";
            break;
        case 6:
            maandnaam = "Juli";
            break;
        case 7:
            maandnaam = "Augustus";
            break;
        case 8:
            maandnaam = "September";
            break;
        case 9:
            maandnaam = "Oktober";
            break;
        case 10:
            maandnaam = "November";
            break;
        case 11:
            maandnaam = "December";
            break;
    }
    return maandnaam;
}

function GetDuur(registratie) {
    var duur = 0;
    if (registratie) {
        var startuur = stringToDate(registratie.Startuur, true);
        var einduur = stringToDate(registratie.Einduur, true);
        var achtuur = new Date(startuur.getFullYear(), startuur.getMonth(), startuur.getDate(), 8, 0);
        var vijfuur = new Date(startuur.getFullYear(), startuur.getMonth(), startuur.getDate(), 17, 0);

        if (startuur < achtuur && einduur <= achtuur) {
            duur = 0
        }
        else if (startuur < achtuur && einduur > achtuur) {
            duur = (((einduur - achtuur) / 1000) / 60) / 60;
        }
        else if (startuur >= vijfuur) {
            duur = 0;
        }
        else if (startuur < vijfuur && einduur > vijfuur) {
            duur = (((vijfuur - startuur) / 1000) / 60) / 60;
        }
        else duur = registratie.Duur;
    }

    return duur;
}

function DayIsComplete(date, Registraties) {
    var totale = 0;
    var dagregistraties = [];
    var strdatum = DateToReverseString(date).trim();
    for (var i = 0; i < Registraties.length; i++) {
        var startdatum = Registraties[i].Startuur.split(" ")[0].trim();
        if (startdatum == strdatum)
            dagregistraties.push(Registraties[i]);
    }

    var dagtotaal = 0;
    $.each(dagregistraties, function (index) {
        dagtotaal += parseFloat(dagregistraties[index].Duur);
    });

    totale = Math.round(dagtotaal * 100) / 100

    return totale;
}

function NoHoles(date, Registraties) {
    var valid = true;
    var aantal = 0;
    var totaleTime = 0;
    var errortext = "Er bevinden zich fouten in de tijdsregistratie!\n";
    var dagregistraties = [];
    var strdatum = DateToReverseString(date).trim();
    for (var i = 0; i < Registraties.length; i++) {
        var startdatum = Registraties[i].Startuur.split(" ")[0].trim();
        if (startdatum == strdatum)
            dagregistraties.push(Registraties[i]);
    }

    for (var i = 0; i < dagregistraties.length; i++) {
        var current = dagregistraties[i];
        var startuur = getTimeFromString(current.Startuur);
        var einduur = getTimeFromString(current.Einduur);
        if (i == 0 && startuur > "08:00") {
            valid = false;
            break;
        }
        if (startuur < "08:00" && einduur >= "08:00") {
            totaleTime += parseFloat(current.Duur);
        }

        if (startuur >= "08:00" && einduur <= "17:00") {
            var next = dagregistraties[i + 1];
            if (next)
                if (getTimeFromString(next.Startuur) <= "17:00") {
                    if (current.Einduur != next.Startuur) {
                        aantal++;
                        errortext += "Registratie " + next.facturatieTekst + " met startuur: " + next.Startuur + "sluit niet aan bij de eindtijd: " + current.Einduur + " van de vorige registratie!\n\n";
                    }
                }
            totaleTime += parseFloat(current.Duur);
        }

        if (startuur <= "17:00" && einduur > "17:00") {
            totaleTime += parseFloat(current.Duur);
        }

    }
    if ((Math.round(totaleTime * 100) / 100) < 8) {
        valid = false;
        errortext += "De totale duur is kleiner dan 8 uur tussen 8 en 17 uur!\n\n";
    }
    if (aantal > 1) {
        valid = false;
    }
    //if (valid == false)
    //    alert(errortext);

    return valid;
}

function getTimeFromString(date) {
    var tijd = date.split(" ")[1];
    var uur = tijd.split(":")[0];
    var minuut = tijd.split(":")[1];
    uur = parseInt(uur) < 10 ? "0" + parseInt(uur) : uur;

    return uur + ":" + minuut;
}

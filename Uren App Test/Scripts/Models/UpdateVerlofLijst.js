﻿function UpdateOveruren(verlofitem) {
    var self = this;

    self.loadList = function () {

        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('VerlofLijst');

        var caml = "<View><ViewFields><FieldRef Name='ID' /><FieldRef Name='Title' /><FieldRef Name='Overuren' /></ViewFields></View>";
        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);

        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var verloflijst = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var vlitem = new VerlofLijstItem();

            vlitem.ID = item.get_item('ID');
            vlitem.Werknemer = item.get_item('Title');
            vlitem.Overuren = item.get_item('Overuren');
            verloflijst.push(vlitem);
        }

        SaveChanges(verloflijst, verlofitem);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load VerlofLijst: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function SaveChanges(verloflijst, item) {
    for (var j = 0; j < verloflijst.length; j++) {
        var verlofitem = verloflijst[j];
        if (verlofitem.Werknemer === item.Werknemer) {
            if (item.Overuren !== "") {
                verlofitem.Overuren += parseFloat(item.Overuren);
                SaveToSharePoint(verlofitem);
                break;
            }
        }
    }    
}

function SaveToSharePoint(myitem) {
    var clientContext = SP.ClientContext.get_current();
    var hostWebContext = new SP.AppContextSite(context, "https://itcbe.sharepoint.com");
    var list = hostWebContext.get_web().get_lists().getByTitle('VerlofLijst');
    var item = list.getItemById(myitem.ID);

    item.set_item('Overuren', myitem.Overuren);

    item.update();
    clientContext.executeQueryAsync(onSaveSucceeded, onSaveFailed);
}

function onSaveSucceeded() {
    GetOveruren();
}

function onSaveFailed(event, args) {
    alert('Fout bij het updaten van de verloflijst!\n' + args.get_message);
}

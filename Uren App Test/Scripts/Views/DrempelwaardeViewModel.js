﻿function GetDrempelwaarde() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Drempelwaarden');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                   + "<Query>"
                       + "<Where>"
                           + "<Eq><FieldRef Name='Title' /><Value Type='Text'>Tijdsregistratie ticket</Value></Eq>"
                       + "</Where>"
                   + "</Query>"
                   + "<ViewFields>"
                      + "<FieldRef Name='Title' />"
                      + "<FieldRef Name='drempelwaarde' />"
                   + "</ViewFields>"
                 + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                Lijst: item.get_item('Title'),
                Drempelwaarde: item.get_item('drempelwaarde').toString()
            });
        }
        SetDrempelwaarden(arraylist);
        GetTijdsregistraties();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function SetDrempelwaarden(arraylist) {
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            if (arraylist[i].Lijst == "Tijdsregistratie ticket") {
                drempelwaardeTijdsregistratie = arraylist[i].Drempelwaarde;
                maxDrempelWaarde = parseInt(drempelwaardeTijdsregistratie) + 5000;
            }
        }
    }
}
﻿var globalRegistraties = [];

function GetAllTijdsregistraties(datum, naam) {
    $("#Loader").show();
    if (naam === "Alle") {
        $('#SaveButton').hide();
    }
    else
        $('#SaveButton').show();
    var mindrempel = 0;
    var maxdrempel = 4999;
    var GetRegis = getTijdsregistraties(datum, naam, mindrempel, maxdrempel);
    Registraties = [];
    GetRegis.done(function () {
        //getOveruren(datum, naam);
    });
}

function getTijdsregistraties(datum, naam, mindrempel, maxdrempel) {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

        var caml = "<View><Query><Where><And><And>";
        if (naam !== "Alle")
            caml += "<And>";
        caml += "<Eq><FieldRef Name='Startuur' /><Value Type='DateTime'>" + datum + "</Value></Eq>"
                + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + mindrempel + "</Value></Gt>"
                + "</And>"
                + "<Leq><FieldRef Name='ID' /><Value Type='Counter'>" + maxdrempel + "</Value></Leq>"
                + "</And>";
        if (naam !== "Alle") {
            caml += "<Eq><FieldRef Name='Author' /><Value Type='Text'>" + naam + "</Value></Eq>"
            + "</And>";
        }
        caml += "</Where>"
                + "<OrderBy><FieldRef Name='Author' /><FieldRef Name='Startuur' /></OrderBy>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Ticket_x0020_getal' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Startuur' />"
                + "<FieldRef Name='Einduur' />"
                + "<FieldRef Name='Duur' />"
                + "<FieldRef Name='Type_x0020_werk' />"
                + "<FieldRef Name='Onsite' />"
                + "<FieldRef Name='Status' />"
                + "<FieldRef Name='Rendabel' />"
                + "<FieldRef Name='Eenheidsprijs' />"
                + "<FieldRef Name='Te_x0020_factureren_x0020_uren' />"
                + "<FieldRef Name='Permanentie' />"
                + "<FieldRef Name='Klant' />"
                + "<FieldRef Name='Opm_x0020_facturatie' />"
                + "<FieldRef Name='Korte_x0020_omschrijving' />"
                + "<FieldRef Name='Author' />"
                + "</ViewFields>"
                + "<RowLimit>1000</RowLimit>"
                + "</View>";

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(
            function () {
                var listEnumerator = _pendingItems.getEnumerator();
                var totaleDuur = 0;
                while (listEnumerator.moveNext()) {
                    var item = listEnumerator.get_current();
                    var startdatum = item.get_item('Startuur');
                    var startuur = DateTimeToString(item.get_item('Startuur'));
                    var einduur = DateTimeToString(item.get_item('Einduur'));
                    var klantitem = item.get_item('Klant');
                    var klant = "";
                    if (klantitem !== undefined && klantitem !== null)
                        klant = klantitem.get_lookupValue();
                    var regis = new Registratie();

                    regis.ID = item.get_item('ID');
                    regis.ticketID = item.get_item('Title');
                    regis.startuur = startuur;
                    regis.einduur = einduur;
                    regis.duur = item.get_item('Duur');
                    regis.facturatieTekst = item.get_item('Korte_x0020_omschrijving');
                    regis.typeWerk = item.get_item('Type_x0020_werk');
                    regis.rendabel = item.get_item('Rendabel');
                    regis.status = item.get_item('Status');
                    regis.onsite = item.get_item('Onsite');
                    regis.permanentie = item.get_item('Permanentie');
                    regis.Eenheidsprijs = item.get_item('Eenheidsprijs');
                    regis.teFacturerenUren = item.get_item('Te_x0020_factureren_x0020_uren');
                    regis.klant = klant;
                    regis.author = item.get_item('Author').get_lookupValue();
                    Registraties.push(regis);
                    totaleDuur += Math.round(item.get_item('Duur') * 100) / 100;
                }
                if (maxdrempel < maxDrempelWaarde) {
                    mindrempel = maxdrempel;
                    maxdrempel += 4999;
                    getTijdsregistraties(datum, naam, mindrempel, maxdrempel);

                }
                else {
                    BindTijdsregistraties(Registraties);
                    ActivateHover();
                    $("#Loader").hide();
                    dfd.resolve();
                }

                //$("#totaalLabel").text(Math.round(totaleDuur * 100) / 100);
            },
            function (sender, args) {
                alert('Unable to load Tijdsregistratie: ' + args.get_message() + '\n' + args.get_stackTrace());
                dfd.reject();
            });

    });
    return dfd.promise();
}

function BindTijdsregistraties(registraties) {
    $("#registratieTBody td").remove();
    $("#registratieTBody tr").remove();
    if (registraties.length > 0) {
        var j = 0;
        var naam = registraties[0].author;
        var totaalPerUser = 0;
        var totaalEffectief = 0;
        var totalerow = "";
        var excludedTickets = ["2", "3", "4", "11", "12"];
        for (var i = 0; i < registraties.length; i++) {
            if (registraties[i].author != naam) {
                totalerow = "<tr><td colspan='8' style='padding:6px; margin: 3px; color: #0171C5; font-size:1.2em;'>" + "Totaal " + naam + ": " + Math.round(totaalPerUser * 100) / 100 + " uren.</td></tr>";
                $("#tijdsregistratieTable").find('tbody').append(totalerow);
                naam = registraties[i].author;
                totaalPerUser = 0;
                totaalEffectief = 0;
            }

            if (registraties[i].status === "Overgehaald") {
                var tablerow = "<tr><td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].ID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].ticketID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'><a href='#' onclick='return OpenRegistratie(" + registraties[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].startuur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].einduur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + Math.round(registraties[i].duur * 100) / 100 + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].facturatieTekst + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].status + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].onsite + "</td>";
                if (registraties[i].rendabel === "Ja")
                    tablerow += "<td style='padding:6px; margin: 3px; color:orange;'><select style='width:60px;'><option value='Ja' selected='selected'>Ja</option><option value='Nee'>Nee</option><option value='null'>Null</option></select></td>";
                else if (registraties[i].rendabel === "Nee")
                    tablerow += "<td style='padding:6px; margin: 3px; color:orange;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee' selected='selected'>Nee</option><option value='null'>Null</option></select></td>";
                else
                    tablerow += "<td style='padding:6px; margin: 3px; color:orange;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee'>Nee</option><option value='null' selected='selected'>Null</option></select></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].permanentie + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].teFacturerenUren + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'><input type='text' style='width:80px;' id='eenheidsprijsTxt' value='" + registraties[i].Eenheidsprijs + "' onkeyup='TeFacturerenUren_KeyUp(this);'/></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].klant + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:orange;'>" + registraties[i].author + "</td>";

                $("#tijdsregistratieTable").find('tbody').append(tablerow);
                totaalPerUser += registraties[i].duur;
                if (excludedTickets.indexOf(registraties[i].ID) === -1)
                    totaalEffectief += registraties[i].duur;

            }
            else if (registraties[i].ticketID == "1") {
                var tablerow = "<tr><td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].ID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].ticketID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'><a href='#' onclick='return OpenRegistratie(" + registraties[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].startuur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].einduur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + Math.round(registraties[i].duur * 100) / 100 + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].facturatieTekst + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].status + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].onsite + "</td>";
                if (registraties[i].rendabel === "Ja")
                    tablerow += "<td style='padding:6px; margin: 3px; color:blue;'><select style='width:60px;'><option value='Ja' selected='selected'>Ja</option><option value='Nee'>Nee</option><option value='null'>Null</option></select></td>";
                else if (registraties[i].rendabel === "Nee")
                    tablerow += "<td style='padding:6px; margin: 3px; color:blue;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee' selected='selected'>Nee</option><option value='null'>Null</option></select></td>";
                else
                    tablerow += "<td style='padding:6px; margin: 3px; color:blue;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee'>Nee</option><option value='null' selected='selected'>Null</option></select></td>";

                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].permanentie + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].teFacturerenUren + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'><input type='text' style='width:80px;' id='eenheidsprijsTxt' value='" + registraties[i].Eenheidsprijs + "' onkeyup='TeFacturerenUren_KeyUp(this);'/></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].klant + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:blue;'>" + registraties[i].author + "</td>";

                $("#tijdsregistratieTable").find('tbody').append(tablerow);
                totaalPerUser += registraties[i].duur;
                if (excludedTickets.indexOf(registraties[i].ID) === -1)
                    totaalEffectief += registraties[i].duur;

            }
            else if (registraties[i].rendabel === "Nee" && registraties[i].status!== "Overgehaald") {
                var tablerow = "<tr><td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].ID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].ticketID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'><a href='#' onclick='return OpenRegistratie(" + registraties[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].startuur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].einduur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + Math.round(registraties[i].duur * 100) / 100 + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].facturatieTekst + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].status + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].onsite + "</td>";
                if (registraties[i].rendabel === "Ja")
                    tablerow += "<td style='padding:6px; margin: 3px; color:red;'><select style='width:60px;'><option value='Ja' selected='selected'>Ja</option><option value='Nee'>Nee</option><option value='null'>Null</option></select></td>";
                else if (registraties[i].rendabel === "Nee")
                    tablerow += "<td style='padding:6px; margin: 3px; color:red;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee' selected='selected'>Nee</option><option value='null'>Null</option></select></td>";
                else
                    tablerow += "<td style='padding:6px; margin: 3px; color:red;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee'>Nee</option><option value='null' selected='selected'>Null</option></select></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].permanentie + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].teFacturerenUren + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'><input type='text' style='width:80px;' id='eenheidsprijsTxt' value='" + registraties[i].Eenheidsprijs + "' onkeyup='TeFacturerenUren_KeyUp(this);'/></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].klant + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:red;'>" + registraties[i].author + "</td>";

                $("#tijdsregistratieTable").find('tbody').append(tablerow);

                totaalPerUser += registraties[i].duur;
                if (excludedTickets.indexOf(registraties[i].ID) === -1)
                    totaalEffectief += registraties[i].duur;

            }
            else if (registraties[i].rendabel === "Ja") {
                var tablerow = "<tr><td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].ID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].ticketID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'><a href='#' onclick='return OpenRegistratie(" + registraties[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].startuur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].einduur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + Math.round(registraties[i].duur * 100) / 100 + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].facturatieTekst + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].status + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].onsite + "</td>";
                if (registraties[i].rendabel === "Ja")
                    tablerow += "<td style='padding:6px; margin: 3px; color:green;'><select style='width:60px;'><option value='Ja' selected='selected'>Ja</option><option value='Nee'>Nee</option><option value='null'>Null</option></select></td>";
                else if (registraties[i].rendabel === "Nee")
                    tablerow += "<td style='padding:6px; margin: 3px; color:green;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee' selected='selected'>Nee</option><option value='null'>Null</option></select></td>";
                else
                    tablerow += "<td style='padding:6px; margin: 3px; color:green;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee'>Nee</option><option value='null' selected='selected'>Null</option></select></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].permanentie + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].teFacturerenUren + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'><input type='text' style='width:80px;' id='eenheidsprijsTxt' value='" + registraties[i].Eenheidsprijs + "' onkeyup='TeFacturerenUren_KeyUp(this);'/></td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].klant + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px; color:green;'>" + registraties[i].author + "</td>";

                $("#tijdsregistratieTable").find('tbody').append(tablerow);
                totaalPerUser += registraties[i].duur;
                if (excludedTickets.indexOf(registraties[i].ID) === -1)
                    totaalEffectief += registraties[i].duur;

            }
            else {

                var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + registraties[i].ID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].ticketID + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenRegistratie(" + registraties[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].startuur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].einduur + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + Math.round(registraties[i].duur * 100) / 100 + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].facturatieTekst + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].status + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].onsite + "</td>";
                if (registraties[i].rendabel === "Ja")
                    tablerow += "<td style='padding:6px; margin: 3px;'><select style='width:60px;'><option value='Ja' selected='selected'>Ja</option><option value='Nee'>Nee</option><option value='null'>Null</option></select></td>";
                else if (registraties[i].rendabel === "Nee")
                    tablerow += "<td style='padding:6px; margin: 3px;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee' selected='selected'>Nee</option><option value='null'>Null</option></select></td>";
                else
                    tablerow += "<td style='padding:6px; margin: 3px;'><select style='width:60px;'><option value='Ja'>Ja</option><option value='Nee'>Nee</option><option value='null' selected='selected'>Null</option></select></td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].permanentie + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].teFacturerenUren + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'><input type='text' style='width:80px;' id='eenheidsprijsTxt' value='" + registraties[i].Eenheidsprijs + "' onkeyup='TeFacturerenUren_KeyUp(this);'/></td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].klant + "</td>";
                tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].author + "</td>";

                $("#tijdsregistratieTable").find('tbody').append(tablerow);
                totaalPerUser += registraties[i].duur;
                if (excludedTickets.indexOf(registraties[i].ID) === -1)
                    totaalEffectief += registraties[i].duur;

            }
        }
        
        totalerow = "<tr><td colspan='8' style='padding:6px; margin: 3px; color: #0171C5; font-size:1.2em;'>" + "Totaal " + naam + " ingegeven: " + Math.round(totaalPerUser * 100) / 100 + " uren.</td></tr>";
        totalerow += "<tr><td colspan='8' style='padding:6px; margin: 3px; color: #0171C5; font-size:1.2em;'>" + "Totaal " + naam + " effectief: " + Math.round(totaalEffectief * 100) / 100 + " uren.</td></tr>";
        $("#tijdsregistratieTable").find('tbody').append(totalerow);

    }
}

function ActivateHover() {
    $("#tijdsregistratieTable tbody tr").hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
}

function OpenRegistratie(id) {
    var url = 'https://itcbe.sharepoint.com/Lists/Tijdsregistratie ticket/Editform.aspx?ID=' + id;
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    return false;
}

function TeFacturerenUren_KeyUp(sender) {
    var waarde = sender.value;
    if (isNaN(waarde))
        alert("De ingegeven text is geen geldig getal!!");
}

function GetRegistratiesFromList() {
    var registraties = [];
    var regisTrs = $('#registratieTBody tr');
    regisTrs.each(function () {
        $tr = $(this);
        var id = $tr.find('td:nth-child(1)').text();
        var rendabel = $tr.find('select').val();
        var prijstxt = $tr.find('#eenheidsprijsTxt').val();
        var prijs = 0;
        if (prijstxt !== "" && !isNaN(prijstxt)) {
            prijs = parseFloat(prijstxt);
        }
        else
            prijs = null;

        registraties.push({
            ID: id,
            Rendabel: rendabel,
            Prijs: prijs
        });
    });

    RegistratiesOpslaan(registraties);
}


function RegistratiesOpslaan(registraties) {
    $("#Loader").show();
    globalRegistraties = registraties;
    if (registraties.length > 0) {
        var index = 0;
        SaveRegistraties(index);
    }
}

function SaveRegistraties(index) {
    var regis = globalRegistraties[index];
    if (regis) {
        var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');
        var item = list.getItemById(regis.ID);

        if (regis.Rendabel === "null")
            regis.Rendabel = null;
        item.set_item("Rendabel", regis.Rendabel);
        item.set_item("Eenheidsprijs", regis.Prijs)

        item.update();
        context.executeQueryAsync(function () { 
            index ++;
            if (index < globalRegistraties.length - 2)
                SaveRegistraties(index);
            else{
                alert("registraties opgeslagen.");
                globalRegistraties = [];
                BerekenButton_Click();
                $("#Loader").hide();
            }

        }, function(sender, args){
            alert("Fout bij het opslaan van registratie " + index + "!!\n" + args.get_message());
        });
    }
}
﻿$(function () {
    SetSelects();
    HidePopup();
    CreateCalendar();
});

function GetEindDatum() {
    var jaar = parseInt($("#jaarSelect").val());
    var maand = parseInt($("#maandSelect").val());
    var DtDatum = new Date(jaar, maand - 1, 1);
    var dag = GetAantaldagen(DtDatum);
    var datum = $("#jaarSelect").val() + "-" + $("#maandSelect").val() + "-" + dag + " T00:00:00Z"
    return datum;
}

function GetBeginDatum() {
    var datum = $("#jaarSelect").val()+ "-" + $("#maandSelect").val() + "-01 T00:00:00Z";
    return datum;
}

function CreateCalendar() {
    var begindatum = GetBeginDatum();
    var einddatum = GetEindDatum();
    var naam = $("#werknemerSelect").val();
    GetOverurenCalendar(begindatum, einddatum, naam, false);
}

function HidePopup() {
    
    $('.popup').slideUp(300, function () { $("#Loader").hide(); });
}

function ShowPopup() {
    $("#Loader").show();
    $('.popup').slideDown(300, function () { });
}

function GetTotaalOveruren(naam) {
    var self = this;

    self.loadlist = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('VerlofLijst');

        var caml = "<View><Query><Where>"
                + "<Eq><FieldRef Name='Title' /><Value Type='Text'>" + naam + "</Value></Eq>"
                + "</Where>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Overuren' />"
                + "</ViewFields>"
                + "<RowLimit>1</RowLimit>"
                + "</View>";

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var Overurenlijst = [];
        var totaal = 0;
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var myItem = item.get_item('Overuren');
            $('#totaalLabel').text("Algemeen totaal: " + myItem + " uren.");
        }

    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load Overuren: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadlist();
}

function GetOverurenCalendar(begindatum, einddatum, naam, details) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Overuren');

        var caml = "<View><Query><Where>"
                + "<And><And>"
                + "<Geq><FieldRef Name='datum' /><Value Type='DateTime'>" + begindatum + "</Value></Geq>"
                + "<Leq><FieldRef Name='datum' /><Value Type='DateTime'>" + einddatum + "</Value></Leq>"
                + "</And>"
                + "<Eq><FieldRef Name='Werknemer' /><Value Type='Text'>" + naam + "</Value></Eq>"
                + "</And>"
                + "</Where>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='datum' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Aantal' />"
                + "<FieldRef Name='Nagekeken' />"
                + "<FieldRef Name='Werknemer' />"
                + "<FieldRef Name='overuren' />"
                + "</ViewFields>"
                + "<RowLimit>1000</RowLimit>"
                + "</View>";

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var Overurenlijst = [];
        var totaal = 0;
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var datum = ToShortDateString(item.get_item('datum').toLocaleString());
            var overuren = new Overuren();

            overuren.ID = item.get_item('ID');
            overuren.Tijdsregistratie_ID = item.get_item('Title') == null ? "" : item.get_item('Title');
            overuren.Aantal = item.get_item('Aantal') == null ? "" : item.get_item('Aantal');
            overuren.Datum = datum;
            overuren.Werknemer = item.get_item('Werknemer').get_lookupValue();
            overuren.Nagekeken = item.get_item('Nagekeken');
            overuren.Overuur = item.get_item('overuren');
            Overurenlijst.push(overuren);

        }
        if (details) {
            
        }
        else {
            
            SetCalendar(Overurenlijst);
            GetTotaalOveruren(naam);
        }
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load Overuren: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function getTijdsregistratiesDetails(datum, naam) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

        var caml = "<View><Query><Where><And>";
        if (naam !== "Alle")
            caml += "<And>";
        caml += "<Eq><FieldRef Name='Startuur' /><Value Type='DateTime'>" + datum + "</Value></Eq>"
                + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>19000</Value></Gt>"
                + "</And>";
        if (naam !== "Alle") {
            caml += "<Eq><FieldRef Name='Author' /><Value Type='Text'>" + naam + "</Value></Eq>"
            + "</And>";
        }
        caml += "</Where>"
                + "<OrderBy><FieldRef Name='Author' /><FieldRef Name='Startuur' /></OrderBy>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Ticket_x0020_getal' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Startuur' />"
                + "<FieldRef Name='Einduur' />"
                + "<FieldRef Name='Duur' />"
                + "<FieldRef Name='Type_x0020_werk' />"
                + "<FieldRef Name='Onsite' />"
                + "<FieldRef Name='Opm_x0020_facturatie' />"
                + "<FieldRef Name='Korte_x0020_omschrijving' />"
                + "<FieldRef Name='Author' />"
                + "</ViewFields>"
                + "<RowLimit>1000</RowLimit>"
                + "</View>";

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var totaleDuur = 0;
        Registraties = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var startuur = GetDateString(item.get_item('Startuur').toLocaleString());
            var einduur = GetDateString(item.get_item('Einduur').toLocaleString());
            var regis = new Registratie();

            regis.ID = item.get_item('ID');
            regis.ticketID = item.get_item('Title');
            regis.startuur = startuur;
            regis.einduur = einduur;
            regis.duur = item.get_item('Duur');
            regis.facturatieTekst = item.get_item('Korte_x0020_omschrijving');
            regis.typeWerk = item.get_item('Type_x0020_werk');
            regis.onsite = item.get_item('Onsite');
            regis.author = item.get_item('Author').get_lookupValue();
            Registraties.push(regis);
            totaleDuur += Math.round(item.get_item('Duur') * 100) / 100;
        }
        ShowDetails(Registraties);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load Tijdsregistratie: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}


function SetCalendar(lijst) {
    var maand = $("#maandSelect").val();
    var jaar = $("#jaarSelect").val();
    var datum = maand + "-01" + "-" + jaar;
    var Datumdate = new Date(datum);
    var aantaldagen = GetAantaldagen(Datumdate);
    var startdag = Datumdate.getDay();
    if (startdag === 0)
        startdag = 7;
    var dag = 1;

    $('.dag').each(function(){
        var $d = $(this);
        var $h4 = $d.find('h4');
        var $h5 = $d.find('h5');
        $h4.text("");
        $h5.text("");

        if ($h5.hasClass("positive"))
            $h5.removeClass("positive");
        if ($h5.hasClass("negative"))
            $h5.removeClass("negative");
    });
    var totaal = 0;
    $("#calendar > tr").each(function () {
        var $tr = $(this);

        for (var i = 0; i < 8; i++) {
            if (dag > aantaldagen)
                break;

            var huidigeDatum = new Date(parseInt(jaar), parseInt(maand) - 1 , parseInt(dag));
            var uren = GetOveruur(huidigeDatum, lijst);

            if (i == startdag) {
                var cell = $tr.find('td:nth-child(' + i + ')');
                cell.find('h4').text(dag);
                if (uren !== undefined) {
                    if (uren != 0) {
                        totaal += parseFloat(uren);
                        var con = cell.find('h5');
                        if (uren >= 0)
                            con.addClass("positive");
                        else
                            con.addClass("negative");
                        con.text(" " + uren + " uren");
                        cell.find('.dag').click(function () {
                            OpenDetails()
                        });
                    }
                }
                startdag++;
                dag++;
            }
        }
        startdag = 1;
    });
    $("#totaalMaandLabel").text("Totaal maand: " + totaal + " uren");
}

function ShowDetails(lijst) {
    $("#detailTable tbody tr").remove();
    var totaal = 0;
    if (lijst.length > 0) {
        for (var i = 0; i < Registraties.length ; i++) {
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + Registraties[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + Registraties[i].ticketID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenRegistratie(" + Registraties[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + Registraties[i].startuur + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + Registraties[i].einduur + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + Math.round(Registraties[i].duur * 100) / 100 + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + Registraties[i].facturatieTekst + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + Registraties[i].author + "</td></tr>";

            $("#detailTable tbody").append(tablerow);
            totaal += parseFloat(Registraties[i].duur);
        }

        var totaalRow = "<tr><td colspan='4' style='padding:6px; margin: 3px; color:#0171C5; font-size:1.2em;'>Totaal uren: " + (Math.round(totaal * 100) / 100) + ".</td></tr>";
        $("#detailTable tbody").append(totaalRow);

        ShowPopup();
    }
}

function OpenRegistratie(id) {
    var url = 'https://itcbe.sharepoint.com/Lists/Tijdsregistratie ticket/Editform.aspx?ID=' + id;
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    return false;
}

function OpenDetails() {
    var e = event || window.event;
    var $d = $(e.srcElement);
    var $h4 = $d.parent().find('h4');
    var dag = $h4.text();
    var maand = $("#maandSelect").val();
    var jaar = $("#jaarSelect").val();
    var datum = jaar + "-" + maand + "-" + dag + " T00:00:00Z";
    var naam = $("#werknemerSelect").val();
    getTijdsregistratiesDetails(datum, naam)
}

function GetOveruur(huidigeDatum, lijst) {
    result = 0;
    var overuren = 0;
    for (var j = 0; j < lijst.length ; j++) {
        var nu = ToShortDateString(huidigeDatum.toLocaleString());
        if (lijst[j].Datum === nu) {
            var uren = lijst[j].Overuur;
            if (uren)
                result = uren;
            break;
        }
    }
    return result;
};

function GetAantaldagen(datum) {
    var dagen = 0;
    var maand = datum.getMonth() + 1;
    if (maand == 2) {
        if (IsSchrikkel(datum.getFullYear())) {
            return 29;
        }
        else return 28;
    }
    else {
        switch (maand) {
            case 1:
                dagen = 31;
                break;
            case 3:
                dagen = 31;
                break;
            case 4:
                dagen = 30;
                break;
            case 5:
                dagen = 31;
                break;
            case 6:
                dagen = 30;
                break;
            case 7:
                dagen = 31;
                break;
            case 8:
                dagen = 31;
                break;
            case 9:
                dagen = 30;
                break;
            case 10:
                dagen = 31;
                break;
            case 11:
                dagen = 30;
                break;
            case 12:
                dagen = 31;
                break;
        }
    }


    return dagen;
}

function IsSchrikkel(jaar) {
    return new Date(jaar, 1, 29).getMonth() == 1;
}

function SetSelects() {
    var vandaag = new Date();
    var maand = vandaag.getMonth() + 1;
    if (maand < 10) {
        maand = "0" + maand;
    }
    var jaar = vandaag.getFullYear();

    $("#maandSelect").val(maand);
    $("#jaarSelect").val(jaar);
}
﻿function GetTechniekers() {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var collGroup = clientContext.get_web().get_siteGroups();
        var oGroup = collGroup.getByName('Medewerkers');
        var collUser = oGroup.get_users();
        clientContext.load(collUser);

        clientContext.executeQueryAsync(
        function () {
            var userInfo = '';
            var userEnumerator = collUser.getEnumerator();
            var arraylist = [];
            while (userEnumerator.moveNext()) {
                var oUser = userEnumerator.get_current();

                arraylist.push(
                {
                    ID: oUser.get_id(),
                    Titel: oUser.get_title(),
                    LoginName: oUser.get_loginName(),
                    Email: oUser.get_email()
                });
            }
            BindTechniekers(arraylist);
            dfd.resolve();
        },
        function (sender, args) {
            alert('Kan de techniekers niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
            dfd.reject(args);
        });
    });
    return dfd.promise();
}

function BindTechniekers(arraylist) {
    $("#medewerkerSelect").empty();
    $('#werknemerSelect').empty();
    for (var i = 0; i < arraylist.length; i++) {
        var option = "<option value='" + arraylist[i].Titel + "'>" + arraylist[i].Titel + "</option>";
        $("#medewerkerSelect").append(option);
        $('#werknemerSelect').append(option);
    }
    $('#medewerkerSelect').prepend("<option></option>");
    $('#werknemerSelect').prepend("<option value='Alle'>Allemaal</option>");
}
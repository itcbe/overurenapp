﻿'use strict';

var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();
var itcGroup;
var usercol;
var drempelwaardeTijdsregistratie;
var maxDrempelWaarde;
var infoText = "";

// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    document.getElementById("ctl00_onetidHeadbnnr2").src = "../Images/ITC_logo.gif";
    document.getElementById("ctl00_onetidHeadbnnr2").width = 80;
    document.getElementById("ctl00_onetidHeadbnnr2").className = "ms-emphasis";
    $('.blok1, .blok2').hide();
    $('#spinner').hide();
    $("#Loader").hide();
    SetDatePickers();
    $('#startdatumTextBox').on('change', function () {
        var datum = $('#startdatumTextBox').val();
        $('#datumTextBox').val(datum);
    })
    getUserName();
});

// This function prepares, loads, and then executes a SharePoint query to get the current users information
function getUserName() {
    context.load(user);
    context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
}

// This function is executed if the above call is successful
// It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {
    $('#message').text('Hello ' + user.get_title());
    var users = GetTechniekers();
    users.done(function () {
        GetDrempelwaarde();
    });
}

// This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    alert('Failed to get user name. Error:' + args.get_message());
}

function SetDatePickers() {
    $.datepicker.setDefaults($.datepicker.regional["nl"]);
    $(".datepicker").datepicker({
        dateFormat: 'dd/mm/yy'
    });
    var vandaag = new Date();
    $(".datepicker").val(DateToString(vandaag));
}

function GetTijdsregistraties() {
    var naam = $('#werknemerSelect').val();
    var datum = ToSharePointDate($('#datumTextBox').val());

    GetAllTijdsregistraties(datum, naam);
    return false;
}

function GetOveruren() {
    var naam = $('#werknemerSelect').val();
    var datum = ToSharePointDate($('#datumTextBox').val());

    getOveruren(datum, naam);
    return false;
}

function HerberekenUren() {
    var rows = $("#OverurenTable tbody tr");
    var totaal = 0;
    rows.each(function () {
        var $tr = $(this);
        var uren = $tr.find("td:nth-child(3)").find("Input").val();
        totaal += parseFloat(uren);
    });
    totaal -= 8;

    $("#overurenTXT").val(totaal);
    return false;
}

/******* Opslaan van de overuren ******/
function GetUserID(succes, error) {
    var oGroup = context.get_web().get_siteGroups();
    itcGroup = oGroup.getByName("ITC");
    usercol = itcGroup.get_users();
    context.load(usercol);

    context.executeQueryAsync(function () {
        var result = [];
        var e = usercol.getEnumerator();
        while (e.moveNext()) {
            result.push(e.get_current());
        }
        succes(result);
    },
    error);
}

function getQueryStringParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function UpdateOverurenInSharePoint(lijst) {
    if (lijst.length > 0) {
        GetUserID(
                function (group) {
                    var teUpdatenLijst = [];
                    var werknemer = "";
                    for (var i = 0; i < lijst.length; i++) {


                        for (var j = 0; j < group.length; j++) {
                            if (group[j].get_title() === lijst[i].Werknemer) {
                                console.log(group[j].get_title() + ', ' + group[j].get_id());
                                werknemer = group[j].get_id();
                            }
                        }
                        if (lijst[i].Nagekeken) {
                            if (lijst[i].ID === "") {
                                var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
                                var list = hostWebContext.get_web().get_lists().getByTitle('Overuren');
                                var itemCreateInfo = new SP.ListItemCreationInformation();
                                var item = list.addItem(itemCreateInfo);

                                item.set_item('Title', lijst[i].Tijdsregistratie_ID);
                                item.set_item('Aantal', lijst[i].Aantal);
                                item.set_item('datum', ToSharePointDate(lijst[i].Datum));
                                item.set_item('Nagekeken', lijst[i].Nagekeken);
                                item.set_item('Werknemer', werknemer);
                                item.set_item('overuren', lijst[i].Overuur);

                                item.update();
                                teUpdatenLijst.push(lijst[i]);

                                //context.executeQueryAsync(onTicketQuerySucceeded, onTicketQueryFailed);
                            }
                            else {
                                var hostWebContext = new SP.AppContextSite(context, "https://itcbe.sharepoint.com");
                                var list = hostWebContext.get_web().get_lists().getByTitle('Overuren');
                                var item = list.getItemById(lijst[i].ID);

                                item.set_item('Title', lijst[i].Tijdsregistratie_ID);
                                item.set_item('Aantal', lijst[i].Aantal);
                                item.set_item('Nagekeken', lijst[i].Nagekeken);
                                item.set_item('overuren', lijst[i].Overuur);

                                item.update();
                            }
                        }
                    }
                    context.executeQueryAsync(onTicketQuerySucceeded(teUpdatenLijst), onTicketQueryFailed);
                },
                function (sender, args) {
                    console.log(args.get_message());
                });
    }
}

function onTicketQuerySucceeded(lijst) {
    alert("Overuren opgeslagen.");
    var verlofitem = new VerlofLijstItem();
    verlofitem.Werknemer = lijst[0].Werknemer;
    verlofitem.Overuren = $("#overurenTXT").val();

    UpdateOveruren(verlofitem);
    //GetTijdsregistraties();
}

function onTicketQueryFailed(sender, args) {
    alert('Fout bij het opslaan van de overuren!\n' + args.get_message());
}

function BerekenButton_Click() {
    infoText = "";
    $("#Loader").show();
    //$('#spinner').show();
    var startdatum = ToSharePointDate($('#startdatumTextBox').val());
    var einddatum = ToSharePointDate($('#einddatumTextBox').val());
    var werknemer = $('#medewerkerSelect').val();
    var minvalue = 0;
    var maxvalue = 4999;
    ClearRendabelInfo();
    GetRegistraties(startdatum, einddatum, werknemer, minvalue, maxvalue);
}

function ClearRendabelInfo() {
    $('#dagDiv').empty();
    $('#dagRendabel').empty();
    $('#salesDiv').empty();
    registraties = [];
    tarieven = [];
    specialePrijzen = [];
    dagopbrengsten = [];
    verplaatsingsKosten = [];
}

function medewerkerSelect_OnChange() {
    var medewerker = $('#medewerkerSelect').val();
    $('#werknemerSelect').val(medewerker);
}

function showInfoDialog() {
    $('#infoTextP').html(infoText);
    $('#dialog').dialog({
        buttons: [{
            text: "OK",
            click: function () {
                $(this).dialog("close");
            }
        }],
        width: 400,
        maxHeight: 600
    });
}






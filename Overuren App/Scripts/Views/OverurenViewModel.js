﻿function getOveruren(datum, naam) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Overuren');

        var caml = "<View><Query><Where>";
        if (naam !== "Alle")
            caml += "<And>";
        caml += "<Eq><FieldRef Name='datum' /><Value Type='DateTime'>" + datum + "</Value></Eq>"
        if (naam !== "Alle") {
            caml += "<Eq><FieldRef Name='Werknemer' /><Value Type='Text'>" + naam + "</Value></Eq>"
            + "</And>";
        }
        caml += "</Where>"
                + "<OrderBy><FieldRef Name='Werknemer' /></OrderBy>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='datum' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Aantal' />"
                + "<FieldRef Name='Nagekeken' />"
                + "<FieldRef Name='Werknemer' />"
                + "</ViewFields>"
                + "<RowLimit>1000</RowLimit>"
                + "</View>";

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var Overurenlijst = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var datum = ToShortDateString(item.get_item('datum').toLocaleString());
            var overuren = new Overuren();

            overuren.ID = item.get_item('ID');
            overuren.Tijdsregistratie_ID = item.get_item('Title') == null ? "" : item.get_item('Title');
            overuren.Aantal = item.get_item('Aantal') == null ? "" : item.get_item('Aantal');
            overuren.Datum = datum;
            overuren.Werknemer = item.get_item('Werknemer').get_lookupValue();
            overuren.Nagekeken = item.get_item('Nagekeken');
            Overurenlijst.push(overuren);
        }
        BindOveruren(Overurenlijst);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load Overuren: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindOveruren(lijst) {
    ClearData();
    var datum = $("#datumTextBox").val();
    var totaal = 0;
    if (lijst.length > 0) {
        for (var i = 0; i < lijst.length; i++) {
            var tablerow = "<tr><td><input type='text' class='IDLabel' id='IdTxt' value='" + lijst[i].ID + "' readonly='readonly' /></td>";
            tablerow += "<td><input type='text' id='RegistratieIdTxt' value='" + lijst[i].Tijdsregistratie_ID + "' /></td>";
            tablerow += "<td><input type='text' class='AantalLabel' id='AantalTxt' value='" + lijst[i].Aantal + "' /></td>";
            tablerow += "<td><input type='text' class='DatumLabel' id='DatumTxt' value='" + $("#datumTextBox").val() + "' readonly='readonly' /></td>";
            if (lijst[i].Nagekeken)
                tablerow += "<td><input type='checkbox' id='NagekekenChk' checked='checked' /></td>";
            else
                tablerow += "<td><input type='checkbox' id='NagekekenChk'/></td>";
            tablerow += "<td><input type='text' class='NaamLabel' id='naamTxt' readonly='readonly' value='" + lijst[i].Werknemer + "' /></td></tr>";
            $("#OverurenTable tbody").append(tablerow);
            totaal += parseFloat(lijst[i].Aantal);
        }
        $("#totaalLabel").text("Totaal :" + (Math.round(totaal * 100) / 100) + " uren");
        $("#overurenTXT").val(Math.round((totaal - 8) * 100) / 100);
    }
    FilterOveruren();
    
    $('.blok1, .blok2').slideDown(500, function () { });
}

function ClearData() {
    $("#OverurenTable tbody tr").each(function () {
        $tr = $(this);
        $tr.remove();
    });
}

function CheckAllAndSave() {
    $("#OverurenTbody").find("tr").each(function () {
        var row = $(this);
        row.find('input[type="checkbox"]').attr('checked', 'checked');
    });
    SaveOveruren();
    return false;
}

function SaveOveruren() {
    var overurenlijst = GetOverurenInput();   
    if (overurenlijst && overurenlijst.length > 0) {
        $('#ErrorLabel').text("");
        UpdateOverurenInSharePoint(overurenlijst)
    }
    else {
        $('#ErrorLabel').text("Opgelet er is foute data ingegeven in de velden Aantal!");
    }
    return false;
}

function FilterOveruren() {
    var keuze = $("#werknemerSelect").val();
    var tabel = $("#OverurenTable tr");
    if (keuze === "Alle") {
        tabel.show();
        HideNagekeken();
    }
    else {
        tabel.filter(function () {
            tabel.hide();

            tabel.filter(function (i, v) {
                var $t = $(this);
                var control = "";
                var nagekeken = $t.find('td:nth-child(5)').find('input');
                var td = $t.find('td:nth-child(6)');
                control = td.find('input').val();
                console.log(control);
                if (control) {
                    if ((control.toLowerCase().indexOf(keuze.toLowerCase())) >= 0 && !nagekeken.is(':checked'))
                        return true;
                    i++;
                }
                else {
                    return true;
                }
                return false;
            }).show();
        });
    }
    HideVerwerkteTijdsregistraties();
}

function HideVerwerkteTijdsregistraties() {
    var registraties = $("#tijdsregistratieTable tbody tr");
    var overuren = $("#OverurenTable tbody tr");
    var GO = [];
    overuren.each(function () {
        var $tr = $(this);
        var id = $tr.find('td:nth-child(2)').find('input').val();
        GO.push(id);
    });
    registraties.hide();
    registraties.filter(function () {
        var $tr = $(this);
        var id = $tr.find('td:nth-child(1)').text();
        if (GO.indexOf(id) > -1) {
            return false;
        }
        else {
            return true;
        }
    }).show();
}

function ShowAllOveruren() {
    var tabel = $("#OverurenTable tr");
    tabel.show();
    return false;
}

function HideNagekeken() {
    var tabel = $("#OverurenTable tr");
    tabel.hide();

    tabel.filter(function (i, v) {
        var $t = $(this);
        var nagekeken = $t.find('td:nth-child(5)').find('input');
        if (!nagekeken.is(':checked'))
            return true;
        return false;
    }).show();

}

function GetOverurenInput() {
    var overurenlijst = [];
    var tdcounter = 1;
    var valid = true;
    $("#OverurenTable tbody tr").each(function () {
        var overuur = new Overuren();
        overuur.Overuur = $("#overurenTXT").val();
        $(this).find('td').each(function () {
            
            var cell = $(this);
            switch (tdcounter) {
                case 1:
                    overuur.ID = cell.find('input[type="text"]').val();
                    break;
                case 2:
                    overuur.Tijdsregistratie_ID = cell.find('input[type="text"]').val();
                    break;
                case 3:
                    overuur.Aantal = cell.find('input[type="text"]').val();
                    if (isNaN(overuur.Aantal))
                        valid = false;
                    break;
                case 4:
                    overuur.Datum = cell.find('input[type="text"]').val()
                    break;
                case 5:
                    overuur.Nagekeken = cell.find('input[type="checkbox"]').is(':checked');
                    break;
                case 6:
                    overuur.Werknemer = cell.find('input[type="text"]').val()
                    break;
            }
            tdcounter++;
        });
        tdcounter = 1;
        overurenlijst.push(overuur);
    });
    if (valid)
        return overurenlijst;
    else
        return valid;
}

